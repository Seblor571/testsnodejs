﻿var __super__ = require("./gameElement.js")
var Constants = require('./constants.js');


var Player = function (pseudo, x, y) {
	this._super = new __super__(Constants.PLAYERWIDTH, Constants.PLAYERHEIGHT, x, y);
	this._pseudo = String(pseudo);
}

Player.prototype.getPseudo = function () {
	return this._pseudo + "test";
}

Player.prototype.toString = function () {
	return '{"Player": {"Pseudo": "' + this._pseudo + '", "inherited":' + this._super + '}}';
}

Player.prototype.toJSON = function () {
	return JSON.parse(this.toString());
}

Player.prototype.equals = __super__.prototype.equals;

module.exports = Player;