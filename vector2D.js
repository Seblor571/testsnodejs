﻿var Exception = require('./exceptions.js');

var Vector2D = function (x, y) {
	if (isNaN(Number(x)))
		throw new Exception("[Vect_C_X_1] Wrong parameter type : expected Number, " + typeof x + " received");
	if (isNaN(Number(y)))
		throw new Exception("[Vect_C_Y_1] Wrong parameter type : expected Number, " + typeof y + " received");
	this._x = x;
	this._y = y;
}

Vector2D.prototype.getX = function () {
	return this._x;
}

Vector2D.prototype.getY = function () {
	return this._y;
}

Vector2D.prototype.toString = function () {
	return '{"Vector2D": {"X": ' + this._x + ', "Y":' + this._y + '}}';
}

Vector2D.prototype.toJSON = function () {
	return JSON.parse(this.toString());
}

module.exports = Vector2D;