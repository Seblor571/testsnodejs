﻿var http = require('http');
var Vector2D = require('./vector2D.js');
var Player = require('./player.js');
var Exception = require('./exceptions.js');
var port = 25042;
console.log();


try {
	
	var p1 = new Player("Seblor", new Vector2D(0, 0));
	var p2 = new Player("Akmenos", new Vector2D(1, 1));

} catch (e) {
	console.error(e._message);
};

http.createServer(function (req, res) {
	res.writeHead(200, { 'Content-Type': 'text/plain' });

    res.end(JSON.stringify(p1.toJSON(), null, 4) + '\n' + p2 + '\n');
}).listen(port);