﻿/**
 * Custom exceptions
 * Containing an ID for error location and a message
 */
var Exceptions = function Exceptions(message) {
	this._message = message;
}

Exceptions.prototype.toString = function () {
	console.error("\texception raised :\n\t\t" + this._message + "\n");
}

module.exports = Exceptions;