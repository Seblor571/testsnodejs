﻿var Exceptions = require('./exceptions.js');

var Indexer = function Indexer() {
	//defining a var instead of this (works for variable & function) will create a private definition
	this._currentIndex = 0;
	
	this.getCurrentIndex = function () {
		return this._currentIndex++;
	};
	
	if (Indexer.caller != Indexer.getInstance) {
		throw new Exceptions("[Indx_C_1] This object cannot be instanciated");
	}
}

/* 
 * SINGLETON CLASS DEFINITION
*/
Indexer.instance = null;

/**
 * Singleton getInstance definition
 * @return singleton class
 */
Indexer.getInstance = function () {
	if (this.instance === null) {
		this.instance = new Indexer();
	}
	return this.instance;
}

module.exports = Indexer.getInstance();