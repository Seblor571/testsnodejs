﻿var Exception = require('./exceptions.js');
var Vector2D = require('./vector2D.js');
var Indexer = require('./indexer.js');

/**
 * x can be a Number or a Vector2D
 */
var GameElement = function (width, height, x, y) {
	if (typeof y === 'undefined') {
		if (!(x instanceof Vector2D))
			throw new Exception("[GE_C_1] Wrong parameter type : expected Vector2D, " + typeof x + " received");
		this._id = Indexer.getCurrentIndex();
		this._location = x;
	}
	else {
		this._location = new Vector2D(x, y);
	}
	this._width = width;
	this._height = height;
}

GameElement.prototype.toString = function () {
	return '{"GameElement": {"id": "' + this._id + '", "location": ' + this._location +
		', "dimensions": {"height": ' + this._height + ', "width": ' + this._width + '}}}';
}

GameElement.prototype.toJSON = function () {
	return JSON.parse(this.toString());
}

GameElement.prototype.equals  = function(ge2) {
	if (!(location instanceof GameElement))
		throw new Exception("[GE_E_1] Wrong parameter type : expected GameElement, " + typeof location + " received");
	return this._id === ge2._id;
}

module.exports = GameElement;